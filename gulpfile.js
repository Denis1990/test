var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	watch = require('gulp-watch'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect');

gulp.task('connect', function() {
  connect.server({
    root: [__dirname],
    livereload: true
  });
});

gulp.task('html', function(){
	gulp.src('*.html')
		.pipe(connect.reload())
})
gulp.task('stylus', function(){
	gulp.src('./css/*.styl')
		.pipe(stylus())
		.pipe(gulp.dest('./css/build'))
		.pipe(connect.reload());
});

gulp.task('watch', function () {
	gulp.watch('css/*.styl', ['stylus'])
	gulp.watch('*.html', ['html'])
});

gulp.task('default', ['connect', 'html', 'watch']);